package salariati.validator;

import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;

public class EmployeeValidator {
	public static boolean isValid(Employee employee) {
		boolean isFirstNameValid  = employee.getFirstName().matches("[a-zA-Z]+") && (employee.getFirstName().length() > 2);
		boolean isLastNameValid  = employee.getLastName().matches("[a-zA-Z]+") && (employee.getLastName().length() > 2);
		boolean isCNPValid       = employee.getCnp().matches("[0-9]+") && (employee.getCnp().length() == 13);
		boolean isFunctionValid  = employee.getFunction().equals(DidacticFunction.ASISTENT) ||
								   employee.getFunction().equals(DidacticFunction.LECTURER) ||
								   employee.getFunction().equals(DidacticFunction.TEACHER) ||
				                   employee.getFunction().equals(DidacticFunction.ASSOCIATE);
		boolean isSalaryValid    = employee.getSalary() > 0;
		
		return isFirstNameValid && isLastNameValid && isCNPValid && isFunctionValid && isSalaryValid;
	}
}
