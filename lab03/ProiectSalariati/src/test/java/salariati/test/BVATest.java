package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class BVATest {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMock();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    public void test_01() {
        assertFalse(controller.getEmployeesList().isEmpty());
        assertEquals(6, controller.getEmployeesList().size());
        Employee newEmployee = new Employee("", "Alexandru", "1234567890123", DidacticFunction.LECTURER, 10900);
        controller.addEmployee(newEmployee);
        assertEquals(6, controller.getEmployeesList().size());
    }

    @Test
    public void test_02() {
        assertFalse(controller.getEmployeesList().isEmpty());
        assertEquals(6, controller.getEmployeesList().size());
        Employee newEmployee = new Employee("1", "Marius", "7894561230123", DidacticFunction.LECTURER, 277);
        controller.addEmployee(newEmployee);
        assertEquals(6, controller.getEmployeesList().size());
    }

    @Test
    public void test_03() {
        assertFalse(controller.getEmployeesList().isEmpty());
        assertEquals(6, controller.getEmployeesList().size());
        Employee newEmployee = new Employee("Taraipan", "Bogdan", "1233211233211", DidacticFunction.TEACHER, 2515);
        controller.addEmployee(newEmployee);
        assertEquals(7, controller.getEmployeesList().size());
    }

    @Test
    public void test_04() {
        String name = "";
        for(Integer i = 0; i < 253; i++)
            name += "M";
        name += "AM";
        assertFalse(controller.getEmployeesList().isEmpty());
        assertEquals(6, controller.getEmployeesList().size());
        Employee newEmployee = new Employee(name, "Marius", "3211233211234", DidacticFunction.ASSOCIATE, 4000);
        controller.addEmployee(newEmployee);
        assertEquals(7, controller.getEmployeesList().size());
    }

    @Test
    public void test_05() {
        assertFalse(controller.getEmployeesList().isEmpty());
        assertEquals(6, controller.getEmployeesList().size());
        Employee newEmployee = new Employee("Psebilschi", "Alexandru", "0987654321321", DidacticFunction.ASISTENT, -100);
        controller.addEmployee(newEmployee);
        assertEquals(6, controller.getEmployeesList().size());
    }
}
