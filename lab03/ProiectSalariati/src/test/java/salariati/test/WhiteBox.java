package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class WhiteBox {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMock();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    public void test_01() {
        controller.clear();
        assertEquals(0, controller.getEmployeesList().size());
        Employee Ionel   = new Employee("Ionel", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2600);

        controller.changeEmployeeFunction(Ionel, DidacticFunction.LECTURER);
    }

    @Test
    public void test_02() {
        assertEquals(6, controller.getEmployeesList().size());
        Employee Ionel   = new Employee("Ionel2", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2600);

        controller.changeEmployeeFunction(Ionel, DidacticFunction.LECTURER);
    }
    @Test
    public void test_03() {
        assertEquals(6, controller.getEmployeesList().size());
        Employee Ionel   = new Employee("Ionel", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2600);
        controller.changeEmployeeFunction(Ionel, DidacticFunction.LECTURER);
    }
}
