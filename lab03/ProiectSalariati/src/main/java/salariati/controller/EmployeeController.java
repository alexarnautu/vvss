package salariati.controller;

import java.util.Comparator;
import java.util.List;

import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

public class EmployeeController {
	
	private EmployeeRepositoryInterface employeeRepository;
	
	public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
	}
	
	public void addEmployee(Employee employee) {
		employeeRepository.addEmployee(employee);
	}
	
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}
	
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		employeeRepository.modifyEmployee(oldEmployee, newEmployee);
	}

	public void deleteEmployee(Employee employee) {
		employeeRepository.deleteEmployee(employee);
	}

	public void changeEmployeeFunction(Employee employee, DidacticFunction didacticFunction) {
		employeeRepository.changeEmployeeFunction(employee, didacticFunction);
	}
	public List<Employee> getSortedEmployeeList() {
		List<Employee> sortedEmployees = employeeRepository.getEmployeeList();
		sortedEmployees.sort(Comparator.comparing(Employee::getSalary).thenComparing(Employee::getAge).reversed());
		return sortedEmployees;
	}

	public void clear() {
	    employeeRepository.clear();
    }
	
}
