package salariati.controller;

import java.util.Comparator;
import java.util.List;

import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

public class EmployeeController {
	
	private EmployeeRepositoryInterface employeeRepository;
	
	public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
	}
	
	public void addEmployee(Employee employee) {
		employeeRepository.addEmployee(employee);
	}
	
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}
	
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		employeeRepository.modifyEmployee(oldEmployee, newEmployee);
	}

	public void deleteEmployee(Employee employee) {
		employeeRepository.deleteEmployee(employee);
	}

//	public List<Employee> getSortedEmployeeList() {
//		return employeeRepository.getEmployeeList().sort(new Comparator<Employee>() {
//			@Override
//			public int compare(Employee o1, Employee o2) {
//				return (o1.getSalary() - o2.getSalary()) ||
//						(Integer.parseInt(o1.getCnp().substring(1,3)) - Integer.parseInt(o2.getCnp().substring(1,3)));
//			}
//		});
//	}
	public List<Employee> getSortedEmployeeList() {
		List<Employee> sortedEmployees = employeeRepository.getEmployeeList();
		sortedEmployees.sort(Comparator.comparing(Employee::getSalary).thenComparing(Employee::getAge).reversed());
		return sortedEmployees;
	}
	
}
