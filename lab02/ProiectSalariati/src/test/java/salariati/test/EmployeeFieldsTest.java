package salariati.test;

import static org.junit.Assert.*;
import salariati.model.Employee;

import org.junit.*;

import salariati.validator.EmployeeValidator;
import salariati.enumeration.DidacticFunction;

public class EmployeeFieldsTest {

	private Employee employee;
	
	@Before
	public void setUp() {
		employee = new Employee("Ioan", "Ardelean", "1234567891234", DidacticFunction.ASISTENT, 1234);
	}

	@Test
	public void testValidFirstName() {
		employee.setFirstName("ValidFirstName");
		assertTrue(EmployeeValidator.isValid(employee));
	}

	@Test
	public void testInvalidFirstName() {
		employee.setFirstName("Invalid#FirstName");
		assertFalse(EmployeeValidator.isValid(employee));
		employee.setFirstName("Invalid!@1");
		assertFalse(EmployeeValidator.isValid(employee));
	}
	
	@Test
	public void testValidLastName() {
		employee.setLastName("ValidLastName");
		assertTrue(EmployeeValidator.isValid(employee));
	}
	
	@Test
	public void testInvalidLastName() {
		employee.setLastName("Invalid#LastName");
		assertFalse(EmployeeValidator.isValid(employee));
		employee.setLastName("Invalid!@1");
		assertFalse(EmployeeValidator.isValid(employee));
	}
	
	@Test
	public void testValidCNP() {
		assertTrue(EmployeeValidator.isValid(employee));
		employee.setCnp("1910509055057");
		assertTrue(EmployeeValidator.isValid(employee));
	}
	
	@Test
	public void testInvalidCNP() {
		employee.setCnp("123456789123");
		assertFalse(EmployeeValidator.isValid(employee));
		employee.setCnp("12345678912345");
		assertFalse(EmployeeValidator.isValid(employee));
		employee.setCnp("123asd456yuio");
		assertFalse(EmployeeValidator.isValid(employee));
		employee.setCnp("ty1234s,.t");
		assertFalse(EmployeeValidator.isValid(employee));
	}
	
	@Test
	public void testValidSalary() {
		assertTrue(EmployeeValidator.isValid(employee));
		employee.setSalary(1500);
		assertTrue(EmployeeValidator.isValid(employee));
	}
	
	@Test
	public void testInvalidSalary() {
		employee.setSalary(-1);
		assertFalse(EmployeeValidator.isValid(employee));
		employee.setSalary(0);
		assertFalse(EmployeeValidator.isValid(employee));
	}

}
