package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BigBang {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMock();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    public void test_A() {
        assertFalse(controller.getEmployeesList().isEmpty());
        assertEquals(6, controller.getEmployeesList().size());
        Employee newEmployee = new Employee("Alexandru", "Arnautu", "1234567890123", DidacticFunction.LECTURER, 10900);
        controller.addEmployee(newEmployee);
        assertEquals(7, controller.getEmployeesList().size());
    }

    @Test
    public void test_B() {
        controller.clear();
        assertEquals(0, controller.getEmployeesList().size());
        Employee Ionel   = new Employee("Ionel", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2600);

        controller.changeEmployeeFunction(Ionel, DidacticFunction.LECTURER);
    }

    @Test
    public void test_C() {
        assertFalse(controller.getEmployeesList().isEmpty());
        List<Employee> sortedList = controller.getSortedEmployeeList();

        Employee firstEmployee = sortedList.get(0);
        Employee lastEmployee = sortedList.get(sortedList.size()-1);
        Employee preLastEmployee = sortedList.get(sortedList.size() -2);
        assertTrue(firstEmployee.getSalary() > lastEmployee.getSalary());
        assertTrue(preLastEmployee.getSalary().equals(lastEmployee.getSalary()));
        assertTrue(preLastEmployee.getAge() < lastEmployee.getAge());
    }

    @Test
    public void test_P() {
        assertEquals(6, controller.getEmployeesList().size());
        Employee newEmployee = new Employee("Alexandru", "Arnautu", "1234567890123", DidacticFunction.LECTURER, 10900);
        controller.addEmployee(newEmployee);
        assertEquals(7, controller.getEmployeesList().size());

        Employee Ionel   = new Employee("Ionel", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2600);

        controller.changeEmployeeFunction(Ionel, DidacticFunction.LECTURER);

        List<Employee> sortedList = controller.getSortedEmployeeList();

        Employee firstEmployee = sortedList.get(0);
        Employee lastEmployee = sortedList.get(sortedList.size()-1);
        Employee preLastEmployee = sortedList.get(sortedList.size() -2);
        assertTrue(firstEmployee.getSalary() > lastEmployee.getSalary());
        assertTrue(preLastEmployee.getSalary().equals(lastEmployee.getSalary()));
        assertTrue(preLastEmployee.getAge() < lastEmployee.getAge());

    }
}
